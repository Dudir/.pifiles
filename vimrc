set nocompatible
set backspace=2
set ruler
set laststatus=2
set showcmd
set showmode
set number 
set visualbell
set autoread 
set noswapfile
set nobackup
set nowb
set smartindent
set smarttab
set shiftwidth=2
set softtabstop=2
set tabstop=2
set expandtab
syntax enable
set background=dark
